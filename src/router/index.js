import Vue from 'vue';
import Router from 'vue-router';
import NoteBook from '@/components/NoteBook';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'NoteBook',
      component: NoteBook,
    },
  ],
});
