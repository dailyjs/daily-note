# Daily Note

https://bitbucket.org/dailyjs/daily-note

## 팀
* **PM** 김승주

* *Vue* : 김장환, 김희상, 박완규
  - https://vuejs.org/
  - https://vuex.vuejs.org/en/
  - https://router.vuejs.org/

* *Electron + API* : 김승주, 안명규, 최은정, 황지섭
  - https://electron.atom.io/docs/tutorial/quick-start/
  - https://github.com/SimulatedGREG/electron-vue
  - https://developer.atlassian.com/bitbucket/api/2/reference/


## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn run dev

# build for production with minification
yarn run build

# launch electron
yarn run electron

# build for production and view the bundle analyzer report
yarn run build --report

# run unit tests
yarn run unit

# run e2e tests
yarn run e2e

# run all tests
yarn test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


