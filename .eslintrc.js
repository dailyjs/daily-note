// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  extends: 'airbnb-base',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // check if imports actually resolve
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'build/webpack.base.conf.js'
      }
    }
  },
  // add your custom rules here
  'rules': {
    // don't require .vue extension when importing
    'import/extensions': ['error', 'always', {
      'mjs': 'never',
      'js': 'never',
      'vue': 'never'
    }],
    // allow optionalDependencies
    'import/no-extraneous-dependencies': ['error', {
      'optionalDependencies': ['test/unit/index.js']
    }],
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,

    //
    "no-console": 0,
    "spaced-comment": 0,
    "no-useless-constructor": 1,
    "new-cap": 0,
    "no-multi-spaces": 0,
    "no-multiple-empty-lines": 1,
    "key-spacing": 0,
    "arrow-body-style": ["warn", "as-needed", { requireReturnForObjectLiteral: true }],
    "one-var": 0,
    "no-use-before-define": 1,
    "dot-notation": 1,
    "no-else-return": 1,
    "no-underscore-dangle": 0,
    "padded-blocks": 1,
    "quotes": 1,
    "comma-dangle": 1,
    "object-shorthand": 1,
    "no-new-object": 0,
    "max-len": ["error", {
      "code": 100,
      "ignoreComments": true,
      "ignorePattern": "^\\s*console\\b"
    }],
    "indent": ["warn", 2, {
      "VariableDeclarator": { "var": 2, "let": 2, "const": 3 },
      "SwitchCase": 1,
    }],
    "computed-property-spacing": 1,
    "brace-style": 1,
    "newline-per-chained-call": 1,
    "no-empty": 1,
    "no-unused-vars": ["warn", {
        "varsIgnorePattern": "^\\s*d?debug\\b",
        "argsIgnorePattern": "^_"
    }],
    "yoda": ["error", "never", {
      "exceptRange": true
    }],

  }
}
